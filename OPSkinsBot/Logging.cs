﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using HipchatApiV2;

namespace OPSkinsBot
{
    public class Logging
    {
        HipchatClient hipClient;
        string logFile;
        StreamWriter stream;

        ConsoleColor[] typeColors;
        string[] typeMsgs;

        public Logging(int sessionId)
        {
            if (!Directory.Exists(Directory.GetCurrentDirectory() + "\\logs\\"))
            {
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\logs\\");
            }

            logFile = Directory.GetCurrentDirectory() + "\\logs\\" + Program.botId + "_" + sessionId + ".log";
            stream = File.AppendText(logFile);
            hipClient = new HipchatClient("UfK20UorX8PMhjjkeVe7D1qHbuEAwlswGm3AdPUi");

            typeColors = new ConsoleColor[sizeof(logType)+1];
            typeMsgs = new string[sizeof(logType)+1];

            typeColors[(int)Convert.ChangeType(logType.DEBUG, logType.DEBUG.GetTypeCode())] = ConsoleColor.Gray;
            typeColors[(int)Convert.ChangeType(logType.WARN, logType.WARN.GetTypeCode())] = ConsoleColor.Yellow;
            typeColors[(int)Convert.ChangeType(logType.ERROR, logType.ERROR.GetTypeCode())] = ConsoleColor.Red;
            typeColors[(int)Convert.ChangeType(logType.FATAL, logType.FATAL.GetTypeCode())] = ConsoleColor.Magenta;
            typeColors[(int)Convert.ChangeType(logType.INFO, logType.INFO.GetTypeCode())] = ConsoleColor.Green;
        
            typeMsgs[(int)Convert.ChangeType(logType.DEBUG, logType.DEBUG.GetTypeCode())] = "[D]";
            typeMsgs[(int)Convert.ChangeType(logType.WARN, logType.WARN.GetTypeCode())] = "[W]";
            typeMsgs[(int)Convert.ChangeType(logType.ERROR, logType.ERROR.GetTypeCode())] = "[E]";
            typeMsgs[(int)Convert.ChangeType(logType.FATAL, logType.FATAL.GetTypeCode())] = "[F]";
            typeMsgs[(int)Convert.ChangeType(logType.INFO, logType.INFO.GetTypeCode())] = "[I]";
            
        }

        ~Logging()
        {
            stream.Close();
        }
        
        public void Log(string message, logType type = logType.INFO, ConsoleColor color = ConsoleColor.White)
        {
            // Get type id
            int typeId = (int)Convert.ChangeType(type, type.GetTypeCode());

            // Log to file
            stream.WriteLine(typeMsgs[typeId] + " " + DateTime.Now.ToString() + ": " + message);
            stream.Flush();

            // Send HipChat Notification
            if (type == logType.ERROR || type == logType.FATAL)
                sendMessageToHipChat(((type == logType.ERROR) ? HipchatApiV2.Enums.RoomColors.Yellow : HipchatApiV2.Enums.RoomColors.Red), message, true);

            // Print to Screen
            Console.ForegroundColor = typeColors[typeId];
            Console.Write(typeMsgs[typeId] + " ");
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.Write(DateTime.Now.ToString() + ": ");
            Console.ForegroundColor = color;
            Console.Write(message + "\n");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void sendMessageToHipChat(HipchatApiV2.Enums.RoomColors color, string message, bool notify)
        {
            try
            {
                //hipClient.SendNotification(1060756, message, color, notify);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to send HipChat Notification. Error: " + ex.Message);
            }
        }
    }
}
