﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace OPSkinsBot
{
    class Database
    {
        static Logging logging;

        private MySqlConnection connection;
        private MySqlCommand getUserSalesQuery;
        private MySqlCommand updateOfferIdQuery;
        private MySqlCommand confirmSalePostedQuery;
        private MySqlCommand confirmItemReturnedQuery;

        private MySqlCommand getCurrentSalesQuery;
        private MySqlCommand getCurrentSalesQueryNoId;
        private MySqlCommand setBotStatusQuery;

        string prefix, table_users, table_sales, table_purchases, table_bot_data;
        string saleColumns;

        public Database(string host, string database, string user, string pass, string ssl, string sPrefix, string sTable_sales, string sTable_users, string sTable_purchases, string sTable_bot_data, Logging lLogging)
        {
            string connectionString = "SERVER=" + host + ";" + "DATABASE=" +
            database + ";" + "UID=" + user + ";" + "PASSWORD=\"" + pass + "\"; SslMode="
            + ssl;

            logging = lLogging;

            prefix = sPrefix;
            table_sales = sTable_sales;
            table_users = sTable_users;
            table_purchases = sTable_purchases;
            table_bot_data = sTable_bot_data;

            saleColumns = "sales.id AS id, sales.id64 AS id64, sales.classid AS classid, sales.instanceid AS instanceid, sales.assigned_bot AS assigned_bot, sales.offer_id AS offer_id, sales.sale_status AS sale_status, sales.timestamp AS timestamp, users.trade_url AS trade_url, purchases.id AS pid, purchases.id64 AS puid, purchases.timestamp AS pts, purchase_user.trade_url AS purchaser_trade_url";

            try
            {
                connection = new MySqlConnection(connectionString);
                connection.Open();

                string query =
                    "SELECT " + saleColumns + " " +
                    "FROM " + database + "." + prefix + "_" + table_sales + " sales " +
                    "INNER JOIN " + database + "." + prefix + "_" + table_users + " users ON sales.id64 = users.id64 " +
                    "LEFT OUTER JOIN " + database + "." + prefix + "_" + table_purchases + " purchases ON sales.id = purchases.item_id " +
                    "LEFT OUTER JOIN " + database + "." + prefix + "_" + table_users + " purchase_user ON purchases.id64 = purchase_user.id64 " +
                    "WHERE sales.id = ?trade_id " +
                    "LIMIT 1;";
                getUserSalesQuery = new MySqlCommand(query, connection);

                query =
                    "UPDATE " + database + "." + prefix + "_" + table_sales + " " +
                    "SET offer_id = ?offer_id " +
                    "WHERE id = ?trade_id;";
                updateOfferIdQuery = new MySqlCommand(query, connection);

                query =
                    "UPDATE " + database + "." + prefix + "_" + table_sales + " " +
                    "SET sale_status = 2, offer_id_old = offer_id, offer_id = 0 " +
                    "WHERE id = ?trade_id AND sale_status = 1;";
                confirmSalePostedQuery = new MySqlCommand(query, connection);

                query =
                    "UPDATE " + database + "." + prefix + "_" + table_sales + " " +
                    "SET sale_status = 6 " +
                    "WHERE id = ?trade_id AND sale_status = 5;";
                confirmItemReturnedQuery = new MySqlCommand(query, connection);

                query =
                    "SELECT " + saleColumns + " " +
                    "FROM " + database + "." + prefix + "_" + table_sales + " sales " +
                    "INNER JOIN " + database + "." + prefix + "_" + table_users + " users ON sales.id64 = users.id64 " +
                    "LEFT OUTER JOIN " + database + "." + prefix + "_" + table_purchases + " purchases ON sales.id = purchases.item_id " +
                    "LEFT OUTER JOIN " + database + "." + prefix + "_" + table_users + " purchase_user ON purchases.id64 = purchase_user.id64 " +
                    "WHERE (sales.sale_status = 1 OR sales.sale_status = 3 OR sales.sale_status = 5) AND sales.offer_id > 0 AND sales.assigned_bot = " + Program.botId + " AND sales.timestamp < ?timestamp;";
                getCurrentSalesQuery = new MySqlCommand(query, connection);

                query =
                    "SELECT " + saleColumns + " " +
                    "FROM " + database + "." + prefix + "_" + table_sales + " sales " +
                    "INNER JOIN " + database + "." + prefix + "_" + table_users + " users ON sales.id64 = users.id64 " +
                    "LEFT OUTER JOIN " + database + "." + prefix + "_" + table_purchases + " purchases ON sales.id = purchases.item_id " +
                    "LEFT OUTER JOIN " + database + "." + prefix + "_" + table_users + " purchase_user ON purchases.id64 = purchase_user.id64 " +
                    "WHERE (sales.sale_status = 1 OR sales.sale_status = 3 OR sales.sale_status = 5) AND sales.offer_id = 0 AND sales.assigned_bot = " + Program.botId + " AND sales.timestamp < ?timestamp;";
                getCurrentSalesQueryNoId = new MySqlCommand(query, connection);

                query =
                    "UPDATE " + database + "." + prefix + "_" + table_bot_data + " " +
                    "SET enabled = ?enabled " +
                    "WHERE id = ?botid;";
                setBotStatusQuery = new MySqlCommand(query, connection);

            } catch (Exception e)
            {
                logging.Log(e.Message);
            }
            
        }

        ~Database()
        {
            connection.Close();
        }

        public void Dispose()
        {
            getUserSalesQuery.Dispose();
            updateOfferIdQuery.Dispose();
            confirmSalePostedQuery.Dispose();
            confirmItemReturnedQuery.Dispose();
            connection.Close();
        }

        #region Queries
        public userSalesTable getUserSales(string id)
        {
            userSalesTable results;

            getUserSalesQuery.Parameters.Clear();
            getUserSalesQuery.Parameters.AddWithValue("?trade_id", id);
            getUserSalesQuery.Prepare();

            //logging.Log(getUserSalesQuery.CommandText);

            DataSet ds = new DataSet();
            MySqlDataAdapter dAdap = new MySqlDataAdapter();
            dAdap.SelectCommand = getUserSalesQuery;
            dAdap.Fill(ds, "result");

            if (ds.Tables["result"].Rows.Count < 1)
            {
                results = new userSalesTable();
                results.trade_id = -1;
                return results;
            }

            salesTableToStruct(ds.Tables["result"].Rows[0], out results);

            return results;
        }

        public List<userSalesTable> getCurrentSales()
        {
            getCurrentSalesQuery.Parameters.Clear();
            getCurrentSalesQuery.Parameters.AddWithValue("?timestamp", Program.getUnixTimestamp(-60).ToString());
            getCurrentSalesQuery.Prepare();

            DataSet ds = new DataSet();
            MySqlDataAdapter dAdap = new MySqlDataAdapter();
            dAdap.SelectCommand = getCurrentSalesQuery;
            dAdap.Fill(ds, "result");

            //logging.Log(getCurrentSalesQuery.CommandText);

            List<userSalesTable> results = new List<userSalesTable>();
            foreach (DataRow dr in ds.Tables["result"].Rows)
            {
                userSalesTable result;
                salesTableToStruct(dr, out result);
                results.Add(result);
            }
            
            return results;
        }

        public List<userSalesTable> getCurrentSalesNoId()
        {
            getCurrentSalesQueryNoId.Parameters.Clear();
            getCurrentSalesQueryNoId.Parameters.AddWithValue("?timestamp", Program.getUnixTimestamp(-60).ToString());
            getCurrentSalesQueryNoId.Prepare();

            DataSet ds = new DataSet();
            MySqlDataAdapter dAdap = new MySqlDataAdapter();
            dAdap.SelectCommand = getCurrentSalesQueryNoId;
            dAdap.Fill(ds, "result");

            //logging.Log(getCurrentSalesQuery.CommandText);

            List<userSalesTable> results = new List<userSalesTable>();
            foreach (DataRow dr in ds.Tables["result"].Rows)
            {
                userSalesTable result;
                salesTableToStruct(dr, out result);
                results.Add(result);
            }

            return results;
        }

        private void salesTableToStruct(DataRow dr, out userSalesTable results)
        {
            results = new userSalesTable();
            try
            {
                results.trade_id = int.Parse(dr["id"].ToString());
                results.user_id = dr["id64"].ToString();
                results.description_id = dr["classid"].ToString() + "_" + dr["instanceid"].ToString();
                results.bot_id = int.Parse(dr["assigned_bot"].ToString());
                results.offer_id = dr["offer_id"].ToString();
                results.sale_status = (saleStatus)dr["sale_status"];
                results.time_stamp = dr["timestamp"].ToString();
                results.trade_url = dr["trade_url"].ToString();
                results.purchaser_trade_url = dr["purchaser_trade_url"].ToString();

                results.purchase_trade_id = dr["pid"].ToString();
                results.purchaser_id = dr["puid"].ToString();
                results.purchase_time_stamp = dr["pts"].ToString();

                string[] parts = results.trade_url.Split('=');
                if (parts.Length == 3)
                    results.token = parts[2];

                parts = results.purchaser_trade_url.Split('=');
                if (parts.Length == 3)
                    results.purchaser_token = parts[2];
            }
            catch (Exception e)
            {
                logging.Log("Error [salesTableToStruct]: " + e.Message, logType.ERROR, ConsoleColor.Yellow);
            }
        }

        public bool updateOfferId(userSalesTable sale, string offer_id)
        {
            sale.offer_id = offer_id;

            updateOfferIdQuery.Parameters.Clear();
            updateOfferIdQuery.Parameters.AddWithValue("?trade_id", sale.trade_id);
            updateOfferIdQuery.Parameters.AddWithValue("?offer_id", sale.offer_id);
            updateOfferIdQuery.Prepare();

            try
            {
                updateOfferIdQuery.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                logging.Log("Error [updateOfferId]: " + e.Message, logType.ERROR, ConsoleColor.Yellow);
                return false;
            }
           
            return true;
        }

        public bool confirmSalePosted(userSalesTable sale)
        {
            sale.sale_status = saleStatus.botHasItems;
            sale.offer_id = "0";

            confirmSalePostedQuery.Parameters.Clear();
            confirmSalePostedQuery.Parameters.AddWithValue("?trade_id", sale.trade_id);
            confirmSalePostedQuery.Prepare();

            try
            {
                confirmSalePostedQuery.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                logging.Log("Error [confirmSalePosted]: " + e.Message, logType.ERROR, ConsoleColor.Yellow);
                return false;
            }

            return true;
        }

        public bool confirmItemReturned(userSalesTable sale)
        {
            sale.sale_status = saleStatus.itemReturned;

            confirmItemReturnedQuery.Parameters.Clear();
            confirmItemReturnedQuery.Parameters.AddWithValue("?trade_id", sale.trade_id);
            confirmItemReturnedQuery.Prepare();

            try
            {
                confirmItemReturnedQuery.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                logging.Log("Error [confirmItemReturned]: " + e.Message, logType.ERROR, ConsoleColor.Yellow);
                return false;
            }

            return true;
        }

        public bool setBotStatus(int botid, bool enabled)
        {
            setBotStatusQuery.Parameters.Clear();
            setBotStatusQuery.Parameters.AddWithValue("?botid", botid);
            setBotStatusQuery.Parameters.AddWithValue("?enabled", (enabled) ? "1" : "0");
            setBotStatusQuery.Prepare();

            try
            {
                setBotStatusQuery.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                logging.Log("Error [setBotStatus]: " + e.Message, logType.ERROR, ConsoleColor.Yellow);
                return false;
            }

            return true;
        }

        #endregion
    }
}

