﻿#region Logging
public enum logType
{
    DEBUG = 0,
    WARN,
    ERROR,
    FATAL,
    INFO
}
#endregion

#region Database
public enum saleStatus
{
    waitingForPickup = 1,
    botHasItems,
    waitingForDelivery,
    botHasDelivered,
    returnRequested,
    itemReturned
}

public struct userSalesTable
{
    public int trade_id;
    public string user_id;
    public string description_id;
    public int bot_id;
    public string offer_id;
    public saleStatus sale_status;
    public string time_stamp;
    public string trade_url;
    public string purchaser_trade_url;

    public string purchase_trade_id;
    public string purchase_time_stamp;
    public string purchaser_id;

    public string token;
    public string purchaser_token;
    //public botStatus bot_status;
}
#endregion

#region SteamBot
enum botStatus
{
    initializing,
    active,
    terminating,
    failed
}

enum botAction
{
    sendOffer,
    confirmOffer,
    manualScan
}

struct botQueuedAction
{
    public botAction action;
    public int argument;
    public int failedAttempts;
    public double timeout;
}
#endregion

#region Response Codes
enum responseCodes
{
    VALID = 100,
    BAD_SYNTAX = 110,
    BAD_ARGUMENT,
    BAD_COMMAND,
    AUTH_MISSING = 120,
    AUTH_INVALID,
    BOT_OFFLINE = 130,
    BOT_TOOBUSY,
    BOT_NOTASSIGNED,
    SALE_UNEXPECTEDSTATUS = 140,
    TIME_OUT = 150,
    UNKNOWN = 199
}

enum SteamErrorCodes
{
	Invalid = 0,
	
	OK = 1,
	Fail = 2,
	NoConnection = 3,
	InvalidPassword = 5,
	LoggedInElsewhere = 6,
	InvalidProtocolVer = 7,
	InvalidParam = 8,
	FileNotFound = 9,
	Busy = 10,
	InvalidState = 11,
	InvalidName = 12,
	InvalidEmail = 13,
	DuplicateName = 14,
	AccessDenied = 15,
	Timeout = 16,
	Banned = 17,
	AccountNotFound = 18,
	InvalidSteamID = 19,
	ServiceUnavailable = 20,
	NotLoggedOn = 21,
	Pending = 22,
	EncryptionFailure = 23,
	InsufficientPrivilege = 24,
	LimitExceeded = 25,
	Revoked = 26,
	Expired = 27,
	AlreadyRedeemed = 28,
	DuplicateRequest = 29,
	AlreadyOwned = 30,
	IPNotFound = 31,
	PersistFailed = 32,
	LockingFailed = 33,
	LogonSessionReplaced = 34,
	ConnectFailed = 35,
	HandshakeFailed = 36,
	IOFailure = 37,
	RemoteDisconnect = 38,
	ShoppingCartNotFound = 39,
	Blocked = 40,
	Ignored = 41,
	NoMatch = 42,
	AccountDisabled = 43,
	ServiceReadOnly = 44,
	AccountNotFeatured = 45,
	AdministratorOK = 46,
	ContentVersion = 47,
	TryAnotherCM = 48,
	PasswordRequiredToKickSession = 49,
	AlreadyLoggedInElsewhere = 50,
	Suspended = 51,
	Cancelled = 52,
	DataCorruption = 53,
	DiskFull = 54,
	RemoteCallFailed = 55,
	PasswordNotSet = 56,
	ExternalAccountUnlinked = 57,
	PSNTicketInvalid = 58,
	ExternalAccountAlreadyLinked = 59,
	RemoteFileConflict = 60,
	IllegalPassword = 61,
	SameAsPreviousValue = 62,
	AccountLogonDenied = 63,
	CannotUseOldPassword = 64,
	InvalidLoginAuthCode = 65,
	AccountLogonDeniedNoMailSent = 66,
	HardwareNotCapableOfIPT = 67,
	IPTInitError = 68,
	ParentalControlRestricted = 69,
	FacebookQueryError = 70,
	ExpiredLoginAuthCode = 71,
	IPLoginRestrictionFailed = 72,
	AccountLocked = 73,
	AccountLogonDeniedVerifiedEmailRequired = 74,
	NoMatchingURL = 75,
	BadResponse = 76,
	RequirePasswordReEntry = 77,
	ValueOutOfRange = 78,
	UnexpectedError = 79,
	Disabled = 80,
	InvalidCEGSubmission = 81,
	RestrictedDevice = 82,
	RegionLocked = 83,
	RateLimitExceeded = 84,
	AccountLogonDeniedNeedTwoFactorCode = 85,
	ItemDeleted = 86,
	AccountLoginDeniedThrottle = 87,
	TwoFactorCodeMismatch = 88,
	NotModified = 91,
}

enum SaleErrorCodes
{
    USER_TIMEOUT = -1,
    USER_MAXTRADES = -2,
    USER_UNTRADABLE = -3,
    USER_BADSTATUS = -4,
    USER_BADTOKEN = -5,
    USER_BADOFFERID = -6,
    FAILED_UNKNOWN = -9,
    AWAITNG_CONFIRMATION = -10
}
#endregion