﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Collections;
using SteamKit2;
using SteamTrade;
using SteamTrade.TradeOffer;
using SteamTrade.TradeWebAPI;
using Newtonsoft.Json;

namespace OPSkinsBot
{
    class SteamBot
    {
        struct outstandingOffer
        {
            public long assetId;
            public string offerId;
        }

        // Bot
        Database dbConnection;
        Logging logging;
        public botStatus status;
        Queue<botQueuedAction> botActions;
        int idleTimer;
        List<outstandingOffer> outstandingOffers = new List<outstandingOffer>();

        System.Timers.Timer manualScanTimer;
        List<string> safeIds = new List<string>();

        // SteamKit2
        SteamClient steamClient;
        SteamFriends steamFriends;
        CallbackManager manager;
        SteamUser steamUser;
        bool userLoggedIn;

        // Steam Trade
        SteamTrading steamTrade;
        TradeOfferManager tradeOfferManager;
        SteamWeb steamWeb;

        public bool isTradeOwner = false;

        // Login details
        string user, pass;
        string authCode, twoFactorAuth;

        string apiKey;
        string MyUniqueId, MyUserNonce;

        public SteamBot(Database db, Logging lLogging, string sUsername, string sPassword, string sApiKey)
        {
            status = botStatus.initializing;
            botActions = new Queue<botQueuedAction>();
            dbConnection = db;
            logging = lLogging;
            idleTimer = 500;
            userLoggedIn = false;

            steamClient = new SteamClient();
            manager = new CallbackManager(steamClient);
            steamUser = steamClient.GetHandler<SteamUser>();
            steamFriends = steamClient.GetHandler<SteamFriends>();
            steamTrade = steamClient.GetHandler<SteamTrading>();

            steamWeb = new SteamWeb();
            ServicePointManager.ServerCertificateValidationCallback += steamWeb.ValidateRemoteCertificate;

            user = sUsername;
            pass = sPassword;
            apiKey = sApiKey;

            new Callback<SteamClient.ConnectedCallback>(OnConnected, manager);
            new Callback<SteamClient.DisconnectedCallback>(OnDisconnected, manager);
            new Callback<SteamUser.LoginKeyCallback>(OnLoginKey, manager);
            new Callback<SteamUser.LoggedOnCallback>(OnLoggedOn, manager);
            new Callback<SteamUser.LoggedOffCallback>(OnLoggedOff, manager);
            new Callback<SteamUser.UpdateMachineAuthCallback>(OnMachineAuth, manager);

            if (Program.settings.get("steam", "onlyweblogin").ToString() == "0")
            {
                steamClient.Connect();

                while (status == botStatus.initializing)
                    manager.RunWaitCallbacks(TimeSpan.FromSeconds(1));
            }
            else
            {
                steamWeb.DoLogin(sUsername, sPassword);
            }

            steamFriends.SetPersonaState(EPersonaState.Online);
            steamFriends.SetPersonaName(Program.settings.get("bot", "friendsname"));

            addJobToQueue(botAction.manualScan, 0);
            manualScanTimer = new System.Timers.Timer(Program.scanRate * 1000);
            manualScanTimer.Elapsed += manualScanTimer_Elapsed;
            manualScanTimer.Enabled = true;
        }

        void manualScanTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            addJobToQueue(botAction.manualScan, 0);
        }

        public void Dispose()
        {
            manualScanTimer.Enabled = false;
            status = botStatus.terminating;
            steamUser.LogOff();
            steamClient.Disconnect();
        }

        #region Queue Related
        public void addJobToQueue(botAction action, int argument, int failedAttempts = 0)
        {
            lock (botActions)
            {
                foreach (botQueuedAction q in botActions)
                {
                    if (q.action == action && q.argument == argument)
                    {
                        logging.Log("Duplicate request, ignoring: " + action.ToString() + ":" + argument);
                        return;
                    }
                }
            }

            botQueuedAction qAction = new botQueuedAction();
            qAction.action = action;
            qAction.argument = argument;
            qAction.failedAttempts = failedAttempts;
            qAction.timeout = Program.getUnixTimestamp(Program.actionTimeout);

            lock (botActions)
            {
                botActions.Enqueue(qAction);
            }
        }

        public void doActions()
        {
            while (status == botStatus.active)
            {
                doNextAction();
            }
        }

        public void doNextAction()
        {
            Thread.Sleep(idleTimer);
            manager.RunCallbacks();
            Program.updateTitle();

            if (!userLoggedIn)
            {
                logging.Log("Bot is not signed in!", logType.WARN);
                return;
            }

            botQueuedAction action = new botQueuedAction();
            bool hasAction = false;

            lock (botActions)
            {
                if (botActions.Count > 0)
                {
                    action = botActions.Dequeue();
                    hasAction = true;

                    logging.Log("Dequeued (" + botActions.Count + "): " + action.action.ToString() + " " + action.argument.ToString() + " " + action.failedAttempts.ToString(), logType.DEBUG);
                }
                else
                {
                    lock (outstandingOffers)
                        outstandingOffers.Clear();
                }
            }

            if (hasAction)
            {
                userSalesTable sale = new userSalesTable();
                SteamID userId = 0;
                TradeOffer offer = null;

                if (action.action == botAction.sendOffer || action.action == botAction.confirmOffer)
                {
                    lock (dbConnection)
                    {
                        sale = dbConnection.getUserSales(action.argument.ToString());
                    }

                    if (sale.trade_id == -1)
                    {
                        logging.Log("No sale found with that ID. Something isn't right...", logType.FATAL);
                        return;
                    }

                    userId = new SteamID(ulong.Parse(sale.user_id));

                    if (sale.bot_id != Program.botId)
                    {
                        logging.Log("This job was not assigned to this bot.", logType.ERROR, ConsoleColor.Green);
                        return;
                    }

                    if (Program.getUnixTimestamp(0) - action.timeout > Program.actionTimeout)
                    {
                        logging.Log("Giving up on " + action.action.ToString() + ":" + action.argument + ". Timed out (" + action.timeout + ").", logType.WARN);

                        if (int.Parse(sale.offer_id) > 0)
                        {
                            if (getOffer(sale.offer_id, out offer))
                            {
                                removeFromOutstanding(offer.TradeOfferId);

                                if (offer.OfferState == TradeOfferState.TradeOfferStateActive)
                                    offer.Cancel();
                                else if (offer.OfferState == TradeOfferState.TradeOfferStateCountered)
                                    offer.Decline();
                            }
                        }

                        lock (dbConnection)
                            dbConnection.updateOfferId(sale, ((int)SaleErrorCodes.USER_TIMEOUT).ToString());
                        return;
                    }
                }

                if (action.failedAttempts > 25)
                {
                    logging.Log("Giving up on " + action.action.ToString() + ":" + action.argument + ". Too many (" + action.failedAttempts + ") failed attempts.", logType.WARN);

                    if (action.action == botAction.sendOffer)
                        lock (dbConnection)
                            dbConnection.updateOfferId(sale, ((int)SaleErrorCodes.FAILED_UNKNOWN).ToString());
                    else if (action.action == botAction.confirmOffer)
                    {
                        if (offer != null && offer.TradeOfferId != null)
                            removeFromOutstanding(offer.TradeOfferId);

                        try
                        {
                            if (offer.OfferState == TradeOfferState.TradeOfferStateActive)
                                offer.Cancel();
                            else if (offer.OfferState == TradeOfferState.TradeOfferStateCountered)
                                offer.Decline();
                        }
                        catch {  }

                        lock (dbConnection)
                            dbConnection.updateOfferId(sale, ((int)SaleErrorCodes.FAILED_UNKNOWN).ToString());
                    }

                    return;
                }

                switch (action.action)
                {
                    case botAction.sendOffer:
                        {
                            idleTimer = 1500;

                            string secToken;
                            long assetId;

                            // appId, contextId, assetId
                            if (sale.sale_status == saleStatus.waitingForPickup)
                            {
                                assetId = getAssetId(userId, sale.description_id);
                                if (assetId == 0)
                                    logging.Log("Got asset id of 0 for : " + sale.description_id + " on " + userId.ConvertToUInt64(), logType.ERROR);

                                offer = tradeOfferManager.NewOffer(userId);
                                offer.Items.AddTheirItem(Program.appId, Program.contextId, assetId);
                                secToken = sale.user_id + sale.time_stamp;
                            }
                            else if (sale.sale_status == saleStatus.waitingForDelivery)
                            {
                                assetId = getAssetId(steamUser.SteamID, sale.description_id);
                                if (assetId == 0)
                                    logging.Log("Got asset id of 0 for : " + sale.description_id + " on BOT", logType.ERROR);

                                SteamID purchaserId;
                                purchaserId = new SteamID(ulong.Parse(sale.purchaser_id));
                                offer = tradeOfferManager.NewOffer(purchaserId);
                                offer.Items.AddMyItem(Program.appId, Program.contextId, assetId);
                                secToken = sale.purchase_trade_id + sale.purchase_time_stamp;
                            }
                            else if (sale.sale_status == saleStatus.returnRequested)
                            {
                                assetId = getAssetId(steamUser.SteamID, sale.description_id);
                                if (assetId == 0)
                                    logging.Log("Got asset id of 0 for : " + sale.description_id + " on BOT", logType.ERROR);

                                offer = tradeOfferManager.NewOffer(userId);
                                offer.Items.AddMyItem(Program.appId, Program.contextId, assetId);
                                secToken = sale.user_id + sale.time_stamp;
                            }
                            else
                            {
                                logging.Log("Unexpected status: " + sale.sale_status.ToString(), logType.ERROR);
                                return;
                            }

                            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(secToken);
                            byte[] hash = Program.md5Service.ComputeHash(inputBytes);
                            secToken = Program.HexStringFromBytes(hash).Substring(0, 6);

                            string offerId;
                            try
                            {

                                if (!offer.SendWithToken(out offerId, ((sale.sale_status == saleStatus.waitingForDelivery) ? sale.purchaser_token : sale.token), sale.trade_id + " - " + Program.tradeMessage + " " + secToken))
                                {
                                    logging.Log("Sending Trade Offer Failed - Status: " + offer.OfferState, color: ConsoleColor.Green);
                                }
                                else
                                {
                                    lock (dbConnection)
                                    {
                                        dbConnection.updateOfferId(sale, offerId);
                                    }
                                    
                                    lock (outstandingOffers)
                                    {
                                        outstandingOffer oOffer = new outstandingOffer();
                                        oOffer.offerId = offer.TradeOfferId;
                                        oOffer.assetId = assetId;

                                        outstandingOffers.Add(oOffer);
                                    }
                                    
                                    addJobToQueue(botAction.confirmOffer, sale.trade_id);

                                    logging.Log("Sent trade offer: " + offerId + " for sale: " + sale.trade_id, logType.DEBUG);
                                }
                            }
                            catch (WebException we)
                            {
                                removeFromOutstanding(offer.TradeOfferId);

                                if (we.Status == WebExceptionStatus.ProtocolError)
                                {
                                    var response = we.Response as HttpWebResponse;
                                    if (response != null)
                                    {
                                        if ((int)response.StatusCode == 401)
                                        {
                                            steamClient.Disconnect();
                                            return;
                                        }
                                        Console.WriteLine("HTTP Status Code: " + (int)response.StatusCode);
                                    }
                                }

                                string resp = new StreamReader(we.Response.GetResponseStream()).ReadToEnd();
                                int errorCode;
                                try
                                {
                                    if (resp.Contains("is not available to trade. More"))
                                    {
                                        logging.Log("User is not available for trade. Canceling trade...", logType.WARN);
                                        lock (dbConnection)
                                            dbConnection.updateOfferId(sale, ((int)SaleErrorCodes.USER_UNTRADABLE).ToString());
                                        return;
                                    }
                                    else if (resp.Contains("because they have a trade ban"))
                                    {
                                        logging.Log("User is trade banned. Canceling trade...", logType.WARN);
                                        lock (dbConnection)
                                            dbConnection.updateOfferId(sale, ((int)SaleErrorCodes.USER_UNTRADABLE).ToString());
                                        return;
                                    }
                                    else
                                    {
                                        errorCode = int.Parse(resp.Split('(')[1].Split(')')[0]);

                                        switch ((SteamErrorCodes)errorCode)
                                        {
                                            case SteamErrorCodes.LimitExceeded:
                                                {
                                                    logging.Log("User has reached maximum active trade offers. Canceling trade...", logType.WARN);
                                                    lock (dbConnection)
                                                        dbConnection.updateOfferId(sale, ((int)SaleErrorCodes.USER_MAXTRADES).ToString());
                                                    return;
                                                }
                                            case SteamErrorCodes.AccessDenied:
                                                {
                                                    logging.Log("User has missing or bad token. Canceling trade...", logType.WARN);
                                                    lock (dbConnection)
                                                        dbConnection.updateOfferId(sale, ((int)SaleErrorCodes.USER_BADTOKEN).ToString());
                                                    return;
                                                }
                                            default:
                                                {
                                                    logging.Log("Unhandled steam error: " + errorCode, logType.ERROR);

                                                    action.failedAttempts = action.failedAttempts + 1;

                                                    lock (botActions)
                                                    {
                                                        botActions.Enqueue(action);
                                                    }

                                                    break;
                                                }
                                        }
                                    }
                                }
                                catch { };
                            }
                            catch (Exception ex)
                            {
                                logging.Log("Error sending trade offer: " + ex.Message + Environment.NewLine + ex.StackTrace, logType.ERROR);
                                action.failedAttempts = action.failedAttempts + 1;

                                lock (botActions)
                                {
                                    botActions.Enqueue(action);
                                }
                            }

                            break;
                        };
                    case botAction.confirmOffer:
                        {
                            if (String.IsNullOrEmpty(sale.offer_id) ||
                                int.Parse(sale.offer_id) <= 0)
                            {
                                logging.Log("Bad or missing offer id for " + sale.trade_id, logType.WARN);

                                lock (dbConnection)
                                    dbConnection.updateOfferId(sale, ((int)SaleErrorCodes.USER_BADOFFERID).ToString());
                                
                                return;
                            }

                            if (sale.sale_status != saleStatus.waitingForDelivery &&
                                sale.sale_status != saleStatus.waitingForPickup &&
                                sale.sale_status != saleStatus.returnRequested)
                            {
                                logging.Log("Unexpected status: " + sale.sale_status.ToString() + " for " + sale.trade_id, logType.WARN);
                                return;
                            }

                            try
                            {
                                if (getOffer(sale.offer_id, out offer))
                                {
                                    if (offer == null)
                                    {
                                        logging.Log("Returned a null offer", logType.FATAL);

                                        action.failedAttempts = action.failedAttempts + 1;

                                        lock (botActions)
                                        {
                                            botActions.Enqueue(action);
                                        }
                                    }

                                    if (offer.OfferState == TradeOfferState.TradeOfferStateAccepted)
                                    {
                                        removeFromOutstanding(offer.TradeOfferId);

                                        if (sale.sale_status == saleStatus.waitingForDelivery)
                                        {
                                            logging.Log("Confirming delivery with server...", logType.DEBUG);
                                            logging.Log("Item Delivery Confirmation: " + confirmDelivery(sale.trade_id));
                                        }
                                        else
                                        {
                                            lock (dbConnection)
                                            {
                                                if (sale.sale_status == saleStatus.waitingForPickup)
                                                {
                                                    dbConnection.confirmSalePosted(sale);
                                                    logging.Log("Sale " + sale.trade_id + " posted!");
                                                }
                                                else if (sale.sale_status == saleStatus.returnRequested)
                                                {
                                                    dbConnection.confirmItemReturned(sale);
                                                    logging.Log("Item in sale " + sale.trade_id + " returned!");
                                                }
                                            }
                                        }
                                    }
                                    else if (offer.OfferState == TradeOfferState.TradeOfferStateActive || offer.OfferState == TradeOfferState.TradeOfferStateInvalidItems)
                                    {
                                        if (offer.OfferState == TradeOfferState.TradeOfferStateInvalidItems)
                                            action.failedAttempts++;

                                        lock (botActions)
                                        {
                                            botActions.Enqueue(action);
                                        }
                                    }
                                    else
                                    {
                                        logging.Log("Trade is in bad state: " + offer.OfferState.ToString() + " for " + sale.sale_status.ToString() + " - " + sale.trade_id);

                                        removeFromOutstanding(offer.TradeOfferId);

                                        if (offer.OfferState == TradeOfferState.TradeOfferStateCountered)
                                            offer.Decline();

                                        lock (dbConnection)
                                        {
                                            dbConnection.updateOfferId(sale, ((int)SaleErrorCodes.USER_BADSTATUS).ToString());
                                        }
                                    }
                                }
                                else
                                {
                                    logging.Log("Unable to find a sent offer: " + sale.offer_id, logType.FATAL, ConsoleColor.Green);
                                }
                            }
                            catch (Exception ex)
                            {
                                logging.Log("Error confirming trade offer: " + ex.Message + Environment.NewLine + ex.StackTrace, logType.ERROR);

                                action.failedAttempts = action.failedAttempts + 1;

                                lock (botActions)
                                {
                                    botActions.Enqueue(action);
                                }
                            }
                            break;
                        };
                    case botAction.manualScan:
                        {
                            logging.Log("Running manual scan...");
                            safeIds.Clear();

                            // Step 1
                            List<userSalesTable> currentSales;
                            lock (dbConnection)
                                currentSales = dbConnection.getCurrentSales();

                            foreach (userSalesTable curSale in currentSales)
                            {
                                addJobToQueue(botAction.confirmOffer, curSale.trade_id);
                                logging.Log("Manual Scan: [C] " + curSale.trade_id, logType.DEBUG);
                                safeIds.Add(curSale.offer_id);
                            }

                            // Step 2
                            lock (dbConnection)
                                currentSales = dbConnection.getCurrentSalesNoId();

                            foreach (userSalesTable curSale in currentSales)
                            {
                                addJobToQueue(botAction.sendOffer, curSale.trade_id);
                                logging.Log("Manual Scan: [S] " + curSale.trade_id, logType.DEBUG);
                                safeIds.Add(curSale.offer_id);
                            }

                            // Step 3
                            try
                            {
                                tradeOfferManager.GetActiveTradeOffers();
                            }
                            catch (Exception ex)
                            {
                                logging.Log("Unable to get active offers: " + ex.Message, logType.ERROR);
                            }

                            logging.Log("Manual Scan Complete!");

                            break;
                        };
                }
            }
            else if (idleTimer < 7000)
            {
                idleTimer += 1000;
            }
        }

        #endregion

        #region Info Retrieval
        public int getQueueSize()
        {
            lock (botActions)
                return botActions.Count;
        }
        
        public bool getOffer(string offerId, out SteamTrade.TradeOffer.TradeOffer tradeOffer)
        {
            try {
                return tradeOfferManager.GetOffer(offerId, out tradeOffer);
            }
            catch (Exception ex)
            {
                logging.Log(ex.Message, logType.ERROR);
            }
            tradeOffer = null;
            return false;
        }

        public void removeFromOutstanding(string offerId)
        {
            if (offerId == null || string.IsNullOrEmpty(offerId))
                return;

            if (outstandingOffers == null)
                return;

            foreach (outstandingOffer offer in outstandingOffers)
            {
                if (offer.offerId == offerId)
                {
                    outstandingOffers.Remove(offer);
                    break;
                }
            }
        }

        public void flushBlackList()
        {
            logging.Log("Manually flushing blacklist", logType.INFO);
            outstandingOffers.Clear();
        }

        public long getAssetId(SteamID user, string descriptionId)
        {
            string classid = descriptionId.Split('_')[0];

            List<GenericInventory.Item> matchedItems = new List<GenericInventory.Item>();

            // Get all the items that match out classid
            GenericInventory botInv = new GenericInventory(steamWeb);
            long[] context = { Program.contextId };
            botInv.load(Program.appId, context, user);
            foreach (KeyValuePair<ulong, GenericInventory.Item> i in botInv.items)
            {
                bool skip = false;
                lock (outstandingOffers)
                {
                    foreach (outstandingOffer oOffer in outstandingOffers)
                    {
                        if (Convert.ToInt64(i.Value.assetid) == oOffer.assetId)
                        {
                            skip = true;
                            break;
                        }
                    }
                }

                if (skip)
                    continue;

                if (i.Value.descriptionid.Split('_')[0].Equals(classid))
                    matchedItems.Add(i.Value);
            }

            // Return closest match
            if (matchedItems.Count == 0)
                return 0;
            else if (matchedItems.Count == 1)
                return Convert.ToInt64(matchedItems[0].assetid);
            else
            {
                try
                {
                    return Convert.ToInt64(getClosestInstanceId(descriptionId, matchedItems).assetid);
                } catch (Exception ex)
                {
                    logging.Log("Unable to getclosetinstanceid: " + ex.Message);
                    return Convert.ToInt64(matchedItems[0].assetid);
                }
            }
        }

        public GenericInventory.Item getClosestInstanceId(string descriptionId, List<GenericInventory.Item> items)
        {
            List<GenericInventory.Item> itemsCopied = new List<GenericInventory.Item>();
            foreach (GenericInventory.Item i in items)
                itemsCopied.Add(i);

            for (int i = 0; i < descriptionId.Length; i ++ )
            {
                foreach (GenericInventory.Item item in items)
                {
                    if (itemsCopied.Contains(item))
                    {
                        if (item.descriptionid[i] != descriptionId[i])
                            itemsCopied.Remove(item);
                    }

                    if (itemsCopied.Count == 1)
                        return itemsCopied[0];
                }
            }

            return itemsCopied[0];
        }

        public ulong getSteam64()
        {
            if (steamClient.IsConnected)
            {
                if (steamUser.SteamID != null)
                    return steamUser.SteamID.ConvertToUInt64();
            }
            return 0;
        }

        #endregion

        #region Callbacks
        void OnConnected(SteamClient.ConnectedCallback callback)
        {
            if (callback.Result != EResult.OK)
            {
                logging.Log("Unable to connect to Steam: " + callback.Result, logType.ERROR, ConsoleColor.Green);

                status = botStatus.failed;
                return;
            }

            logging.Log("Connected to Steam! Logging in '" + user + "'...", color: ConsoleColor.Green);

            byte[] sentryHash = null;
            if (File.Exists("sentries/sentry-" + user + ".bin"))
            {
                // if we have a saved sentry file, read and sha-1 hash it
                byte[] sentryFile = File.ReadAllBytes("sentries/sentry-" + user + ".bin");
                sentryHash = CryptoHelper.SHAHash(sentryFile);
            }

            steamUser.LogOn(new SteamUser.LogOnDetails
            {
                Username = user,
                Password = pass,
                AuthCode = authCode,
                TwoFactorCode = twoFactorAuth,
                SentryFileHash = sentryHash,
            });
        }

        void OnDisconnected(SteamClient.DisconnectedCallback callback)
        {
            logging.Log("Disconnected from Steam, reconnecting in 5...", color: ConsoleColor.Green);

            userLoggedIn = false;

            Thread.Sleep(TimeSpan.FromSeconds(5));

            steamClient.Connect();
        }

        void OnLoginKey(SteamUser.LoginKeyCallback callback)
        {
            MyUniqueId = callback.UniqueID.ToString();

            bool authd = steamWeb.Authenticate(MyUniqueId, steamClient, MyUserNonce);

            if (authd)
            {
                tradeOfferManager = new TradeOfferManager(apiKey, steamWeb);
                tradeOfferManager.OnNewTradeOffer += OnNewTradeOffer;

                userLoggedIn = true;
                status = botStatus.active;
            }
            else
            {
                logging.Log("Web Authentication failed, retrying in 5s...", logType.ERROR, color: ConsoleColor.Green);
                steamClient.Disconnect();
                Thread.Sleep(5000);
            }
        }

        void OnLoggedOn(SteamUser.LoggedOnCallback callback)
        {
            bool isSteamGuard = callback.Result == EResult.AccountLogonDenied;
            bool is2FA = callback.Result == EResult.AccountLogonDeniedNeedTwoFactorCode;

            if (isSteamGuard || is2FA)
            {
                logging.Log("This account is SteamGuard protected!", color: ConsoleColor.Green);

                if (is2FA)
                {
                    Console.Write("Please enter your 2 factor auth code from your authenticator app: ");
                    twoFactorAuth = Console.ReadLine();
                }
                else
                {
                    Console.Write("Please enter the auth code sent to the email at {0}: ", callback.EmailDomain);
                    authCode = Console.ReadLine();
                }

                return;
            }

            if (callback.Result != EResult.OK)
            {
                logging.Log("Unable to logon to Steam: " + callback.Result + "/" + callback.ExtendedResult, logType.ERROR, ConsoleColor.Green);

                steamClient.Disconnect();
                return;
            }

            MyUserNonce = callback.WebAPIUserNonce;

            logging.Log("Successfully logged on!", color: ConsoleColor.Green);
        }

        void OnLoggedOff(SteamUser.LoggedOffCallback callback)
        {
            logging.Log("Logged off of Steam: " + callback.Result, color: ConsoleColor.Green);
            userLoggedIn = false;
        }

        void OnMachineAuth(SteamUser.UpdateMachineAuthCallback callback)
        {
            logging.Log("Updating sentryfile...", color: ConsoleColor.Green);

            byte[] sentryHash = CryptoHelper.SHAHash(callback.Data);
            File.WriteAllBytes("sentries/sentry-" + user + ".bin", callback.Data);

            steamUser.SendMachineAuthResponse(new SteamUser.MachineAuthDetails
            {
                JobID = callback.JobID,

                FileName = callback.FileName,

                BytesWritten = callback.BytesToWrite,
                FileSize = callback.Data.Length,
                Offset = callback.Offset,

                Result = EResult.OK,
                LastError = 0,

                OneTimePassword = callback.OneTimePassword,

                SentryFileHash = sentryHash,
            });

            logging.Log("Steam Guard setup complete!", color: ConsoleColor.Green);
        }

        public void OnNewTradeOffer(TradeOffer offer)
        {
            if (!offer.IsOurOffer)
            {
                logging.Log("Declining Trade Offer [NO]: " + offer.TradeOfferId + " from " + offer.PartnerSteamId, color: ConsoleColor.Green);
                removeFromOutstanding(offer.TradeOfferId);
                
                offer.Decline();
            }

            if (offer.OfferState == TradeOfferState.TradeOfferStateActive)
            {
                if (safeIds.Contains(offer.TradeOfferId))
                    return;

                logging.Log("Declining Trade Offer [A]: " + offer.TradeOfferId + " from " + offer.PartnerSteamId, color: ConsoleColor.Green);

                removeFromOutstanding(offer.TradeOfferId);

                offer.Cancel();
            }
            else if (offer.OfferState == TradeOfferState.TradeOfferStateCountered)
            {
                logging.Log("Declining Trade Offer [C]: " + offer.TradeOfferId + " from " + offer.PartnerSteamId, color: ConsoleColor.Green);
                offer.Decline();
            }
        }
        #endregion

        #region Manual WebRequests
        public string makePostRequest(string cUrl, string cPostData)
        {
            HttpWebRequest request;
            HttpWebResponse response;

            request = (HttpWebRequest)WebRequest.Create(cUrl);
            request.Host = "opskins.com";
            request.Accept = "*/*";
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36";
            request.AllowAutoRedirect = true;
            request.ContentType = "application/x-www-form-urlencoded;";
            //request.Referer = ".com";
            request.ContentLength = cPostData.Length;
            //request.CookieContainer = new CookieContainer();
            //request.CookieContainer.SetCookies(new Uri("http://steamcommunity.com/"), "----------");
            request.Method = "POST";
            request.KeepAlive = true;

            Stream requestStream = request.GetRequestStream();
            Byte[] postBytes = Encoding.UTF8.GetBytes(cPostData);
            requestStream.Write(postBytes, 0, postBytes.Length);
            requestStream.Close();

            try
            {
                response = (HttpWebResponse)request.GetResponse();
                Stream ReceiveStream = response.GetResponseStream();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(ReceiveStream, encode);
                char[] read = new char[256];
                int count = readStream.Read(read, 0, 256);
                string all = "";
                while (count > 0)
                {
                    string str = new string(read, 0, count);
                    all = all + str;
                    count = readStream.Read(read, 0, 256);
                }
                readStream.Close();
                response.Close();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    logging.Log("Failed to send invite: " + response.StatusCode, logType.ERROR, ConsoleColor.Green);
                }

                return all;
            }
            catch (Exception ex)
            {
                logging.Log("Failed to get response: " + ex.Message, logType.ERROR, ConsoleColor.Green);
                return "FAIL";
            }
        }

        public responseCodes confirmDelivery(int id)
        {
            string hash = id + Program.botSalt;
            byte[] hashBytes = Encoding.UTF8.GetBytes(hash);
            hashBytes = Program.sha1Service.ComputeHash(hashBytes);

            string response = makePostRequest(Program.confirm_url, "id=" + id + "&hash=" + Program.HexStringFromBytes(hashBytes));


            int responseCode = 0;
            try
            {
                responseCode = int.Parse(response);
            }
            catch
            {
                logging.Log(response, logType.FATAL);
                return responseCodes.UNKNOWN;
            }

            return (responseCodes)responseCode;
        }
        #endregion
    }
}
