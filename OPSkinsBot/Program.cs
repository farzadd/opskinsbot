﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using Ini;

namespace OPSkinsBot
{
    class Program
    {
        #region "Code to handle exit"
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

        private delegate bool EventHandler(CtrlType sig);
        static EventHandler _handler;

        enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        private static bool Handler(CtrlType sig)
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            logging.Log("EXIT PROCEDURE BLOCKED", logType.WARN);
            logging.Log("NOTE: Use exit/shutdown command to safely shutdown the bot", logType.WARN);
            safeExit(true);
            return false;
        }
        #endregion

        // Objects
        public static IniFile settings;
        private static Database dbConnection;
        private static SteamBot steamBot;
        private static Logging logging;

        // Threads
        public static TcpListener listenServer;
        public static Thread listenThread;

        // Settings
        public static string settingsFile = null;

        public static int botId;
        public static string tradeMessage;
        public static string botSalt;
        public static int loglevel;
        public static int appId;
        public static long contextId;
        public static int sessionId;
        public static string confirm_url;
        public static int actionTimeout;
        public static int scanRate;

        public static bool exit;
        public static bool isBotActive;

        // Security
        public static SHA1 sha1Service;
        public static MD5 md5Service;

        static void Main(string[] args)
        {
            try
            {
                // Setup unhandled exception catcher
                AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;

                printConsoleHeader(true);

                // Initialize the bot
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                if (args.Length > 0)
                    settingsFile = args[0];

                while (settingsFile == null || !File.Exists(Directory.GetCurrentDirectory() + "\\" + settingsFile + ".ini"))
                {
                    if (settingsFile != null)
                        Console.WriteLine("Bot config file not found. Try again...");

                    Console.Write(Environment.NewLine + "Please enter the bot config to execute (i.e bot1): ");
                    settingsFile = Console.ReadLine();
                }

                printConsoleHeader(true);
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Initializing bot...");

                settings = new IniFile(Directory.GetCurrentDirectory() + "\\" + settingsFile + ".ini");

                botId = int.Parse(settings.get("bot", "id"));
                tradeMessage = settings.get("bot", "trademessage");
                loglevel = int.Parse(settings.get("bot", "loglevel"));
                actionTimeout = int.Parse(settings.get("bot", "actiontimeout"));
                scanRate = int.Parse(settings.get("bot", "scanrate"));

                appId = int.Parse(settings.get("steam", "appid"));
                contextId = long.Parse(settings.get("steam", "contextid"));
                sessionId = int.Parse(settings.get("auto", "sessionid"));
                confirm_url = settings.get("website", "confirm_url");
                botSalt = settings.get("website", "salt");

                settings.set("auto", "safeexit", "0");
                settings.set("auto", "sessionid", (sessionId + 1).ToString());

                logging = new Logging(sessionId);

                Console.Title = "OPSkins Bot #" + botId;

                sha1Service = new SHA1CryptoServiceProvider();
                md5Service = new MD5CryptoServiceProvider();

                _handler += new EventHandler(Handler);
                SetConsoleCtrlHandler(_handler, true);

                isBotActive = true;
                exit = false;

                // Bot Initialized
                Thread.Sleep(1000);
                printConsoleHeader(true);
                logging.Log("Bot (#" + botId + ") initialized!");

                // Connect to the Mysql Database
                logging.Log("Connecting to Database (" + settings.get("database", "hostname") + ")...", color: ConsoleColor.Yellow);
                dbConnection = new Database(settings.get("database", "hostname"),
                    settings.get("database", "database"),
                    settings.get("database", "username"),
                    settings.get("database", "password"),
                    settings.get("database", "sslmode"),
                    settings.get("database", "prefix"),
                    settings.get("database", "db_table_sales"),
                    settings.get("database", "db_table_users"),
                    settings.get("database", "db_table_purchases"),
                    settings.get("database", "db_table_bot_data"),
                    logging);
                logging.Log("Database initialization completed!", color: ConsoleColor.Yellow);

                // Setup the steam client
                logging.Log("Initializing Steam Client...", color: ConsoleColor.Green);
                steamBot = new SteamBot(dbConnection,
                    logging,
                    settings.get("steam", "username"),
                    settings.get("steam", "password"),
                    settings.get("steam", "apikey"));
                logging.Log("Steam Client initialization completed!", color: ConsoleColor.Green);

                // Create a thread to listen for incoming requests
                listenThread = new Thread(new ThreadStart(startListening));
                listenThread.Start();

                // Create a thread to run requests
                Thread botThread = new Thread(new ThreadStart(steamBot.doActions));
                botThread.Start();

                // Set the bots stauts in the DB
                dbConnection.setBotStatus(botId, true);

                // Send HipChat Notification
                logging.sendMessageToHipChat(HipchatApiV2.Enums.RoomColors.Green, "Bot #" + botId + " is online!", false);

                // Loop
                while (!exit)
                {
                    String input = Console.ReadLine();
                    handleCommand(input);
                }
            }
            catch (Exception majorException)
            {
                CrashRestart(majorException);
            }
        }

        static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
        {
            CrashRestart((Exception)e.ExceptionObject);
        }

        static void CrashRestart(Exception majorException)
        {
            // Generate error log
            string errorString = majorException.Message + Environment.NewLine;
            errorString += "================================STACK TRACE================================" + Environment.NewLine;
            errorString += majorException.StackTrace + Environment.NewLine;
            errorString += "================================INNER EXCEPTION================================" + Environment.NewLine;
            errorString += majorException.InnerException + Environment.NewLine;
            errorString += "================================SOURCE================================" + Environment.NewLine;
            errorString += majorException.Source + Environment.NewLine;
            errorString += "================================TARGET SITE================================" + Environment.NewLine;
            errorString += majorException.TargetSite + Environment.NewLine;
            errorString += "================================MANUAL================================" + Environment.NewLine;
            errorString += "Logging file: " + sessionId + Environment.NewLine;
            Random rEng = new Random();
            File.WriteAllText(Directory.GetCurrentDirectory() + "\\logs\\crashdump_" + DateTime.Now.ToString("dmy-hhmmss") + "_" + rEng.Next(100, 999) + ".log", errorString);

            try
            {
                dbConnection.setBotStatus(botId, false);
            }
            catch { };

            // Restart
            Console.WriteLine("");
            Console.WriteLine("A fatal error has occured. An error report has been generated. The bot will now restart...");
            Thread.Sleep(3000);
            System.Diagnostics.Process.Start(Assembly.GetExecutingAssembly().Location, settingsFile);
            Environment.Exit(0);
        }

        #region Listening for requests
        public static void startListening()
        {
            Int32 port = Int32.Parse(settings.get("website", "listenport"));
            listenServer = new TcpListener(IPAddress.Any, port);

            try
            {
                listenServer.Start();

                // Buffer for reading data
                byte[] bytes = new byte[1024];
                String data = null;

                logging.Log("Listen server is active!");
                // Enter the listening loop. 
                while (true)
                {
                    logging.Log("Waiting for remote request...", logType.DEBUG);
                    TcpClient client = null;
                    try
                    {
                        client = listenServer.AcceptTcpClient();
                    } catch (SocketException se) {
                        if (!se.Message.Contains("WSACancel"))
                            logging.Log(se.Message);
                        return;
                    }
                    logging.Log("Connection Established!", logType.DEBUG);

                    string clientIP = client.Client.RemoteEndPoint.ToString().Split(':')[0];
                    string[] whitelistIps = settings.get("website", "listenip").ToString().Split(';');

                    if (!whitelistIps.Contains(clientIP))
                    {
                        logging.Log("IP: '" + clientIP + "' does not match website IP. Ignoring request...", logType.WARN);
                        client.Close();
                        continue;
                    }

                    int i;
                    data = null;
                    NetworkStream stream = client.GetStream();

                    // Loop to receive all the data sent by the client. 
                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                        logging.Log("Received request: " + data, color: ConsoleColor.Yellow);

                        string response = handleCommand(data, true);

                        // Send back a response.
                        byte[] msg = System.Text.Encoding.ASCII.GetBytes(response);
                        stream.Write(msg, 0, msg.Length);

                        // Close the connection - shit code, fix later
                        client.Close();
                        break;
                    }
                }
            } catch (ThreadAbortException)
            {
                listenServer.Stop();
                Thread.ResetAbort();
            }
        }
        #endregion

        public static string handleCommand(String command, bool remote = false)
        {
            string[] parts = command.Split(',');

            if (remote)
            {
                if (parts.Length < 3)
                {
                    logging.Log(responseCodes.AUTH_MISSING.ToString(), logType.ERROR, ConsoleColor.DarkYellow);
                    return ((int)responseCodes.AUTH_MISSING).ToString();
                }

                string hash = parts[0] + "," + parts[1] + "," + botSalt;
                byte[] hashBytes = Encoding.UTF8.GetBytes(hash);
                hashBytes = sha1Service.ComputeHash(hashBytes);

                if (!parts[2].Equals(HexStringFromBytes(hashBytes).Substring(0, 5)))
                {
                    logging.Log(responseCodes.AUTH_INVALID.ToString() + " - Local: " + HexStringFromBytes(hashBytes).Substring(0, 5), logType.ERROR, ConsoleColor.DarkYellow);
                    return ((int)responseCodes.AUTH_INVALID).ToString();
                }
            }

            int arg = 0;
            try
            {
                if (parts.Length >= 2)
                {
                    if (!string.IsNullOrEmpty(parts[1].Trim().ToString()))
                        arg = int.Parse(parts[1]);
                }
            } catch (Exception ex)
            {
                logging.Log(responseCodes.BAD_SYNTAX.ToString() + ": " + ex.Message, logType.ERROR, ConsoleColor.DarkYellow);
                return ((int)responseCodes.BAD_SYNTAX).ToString();
            }

            if (parts[0].Equals("version"))
            {
                if (remote)
                    return ((int)responseCodes.VALID).ToString() + "," + Assembly.GetExecutingAssembly().GetName().Version.ToString().Replace('.', '\0');
                else
                    logging.Log("Version: " + Assembly.GetExecutingAssembly().GetName().Version, color: ConsoleColor.DarkYellow);
            }
            else if (parts[0].Equals("sendoffer"))
            {
                steamBot.addJobToQueue(botAction.sendOffer, arg);
            }
            else if (parts[0].Equals("confirmoffer"))
            {
                steamBot.addJobToQueue(botAction.confirmOffer, arg);
            }
            else if (parts[0].Equals("changestatus"))
            {
                if (arg != 0 && arg != 1)
                {
                    logging.Log(responseCodes.BAD_ARGUMENT.ToString(), logType.ERROR, ConsoleColor.DarkYellow);
                    return ((int)responseCodes.BAD_ARGUMENT).ToString();
                }

                if ((isBotActive && arg == 1) || (!isBotActive && arg == 0))
                {
                    logging.Log(responseCodes.BAD_ARGUMENT.ToString(), logType.ERROR, ConsoleColor.DarkYellow);
                    return ((int)responseCodes.BAD_ARGUMENT).ToString();
                }

                if (arg == 1)
                    isBotActive = true;
                else
                    isBotActive = false;
            }
            else if (parts[0].Equals("status"))
            {
                if (isBotActive)
                {
                    logging.Log("Bot is ACTIVE", color: ConsoleColor.DarkYellow);
                    return ((int)responseCodes.VALID).ToString() + ",1";
                }
                else
                {
                    logging.Log("Bot is INACTIVE", color: ConsoleColor.DarkYellow);
                    return ((int)responseCodes.VALID).ToString() + ",0";
                }
            }
            else if (parts[0].Equals("manualscan"))
            {
                steamBot.addJobToQueue(botAction.manualScan, arg);
            }
            else if (parts[0].Equals("steamid"))
            {
                string steamId = steamBot.getSteam64().ToString();
                logging.Log("Steam ID:" + steamId, color: ConsoleColor.DarkYellow);
                return ((int)responseCodes.VALID).ToString() + "," + steamId;
            }
            else if (parts[0].Equals("debug") && !remote)
            {
                logging.Log("Debug information dump:", color: ConsoleColor.DarkYellow);

                return ((int)responseCodes.VALID).ToString();
            }
            else if (parts[0].Equals("clear"))
            {
                printConsoleHeader(true);
                return "";
            }
            else if (parts[0].Equals("echo"))
            {
                logging.Log(parts[1], color: ConsoleColor.DarkYellow);
                return parts[1];
            }
            else if (!remote && (parts[0].Equals("exit") || parts[0].Equals("shutdown")))
            {
                safeExit();
            }
            else if (!remote && (parts[0].Equals("flush")))
            {
                steamBot.flushBlackList();
            }
            else
            {
                logging.Log("Invalid command issued: " + parts[0], logType.WARN, ConsoleColor.DarkYellow);
                return ((int)responseCodes.BAD_COMMAND).ToString();
            }

            logging.Log(responseCodes.VALID.ToString(), color: ConsoleColor.DarkYellow);
            return ((int)responseCodes.VALID).ToString();
        }

        public static void printConsoleHeader(bool clear = false)
        {
            if (clear)
                Console.Clear();

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("                 =============================================");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("                 ===   OPSkins Steam Bot - V." + Assembly.GetExecutingAssembly().GetName().Version + "   ===");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("                 =============================================");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void updateTitle()
        {
            Console.Title = "OPSkins Bot #" + botId + " | SID: " + sessionId.ToString("D5") + " | QS: " + ((steamBot == null) ? "---" : steamBot.getQueueSize().ToString("D3")) + " | T: " + DateTime.Now.ToString("hh:mm:ss tt");
        }

        public static void safeExit(bool force = false)
        {
            // Send hipchat notification
            logging.sendMessageToHipChat(HipchatApiV2.Enums.RoomColors.Purple, "Bot #" + botId + "  exiting safely!", true);

            // Disable the bot
            dbConnection.setBotStatus(botId, false);
            isBotActive = false;

            // Wait for the queue to empty
            logging.Log("Exiting safely. Waiting for queue to empty...", color: ConsoleColor.Red);
            while (steamBot.getQueueSize() > 0)
                Thread.Sleep(500);

            // Terminate the queue
            steamBot.status = botStatus.terminating;

            // Exit all the extra threads
            logging.Log("Exiting all threads...", color: ConsoleColor.Red);
            if (listenServer != null)
            {
                listenServer.Stop();
                listenThread.Abort();
            }
            
            // Close the DB connection
            logging.Log("Closing database connection...", color: ConsoleColor.Red);
            if (dbConnection != null)
                dbConnection.Dispose();

            // Log out and close steam connection
            logging.Log("Logging out and disconnecting from Steam...", color: ConsoleColor.Red);
            if (steamBot != null)
                steamBot.Dispose();

            // Record safe exit procedure
            settings.set("auto", "safeexit", "1");

            // Close in 3 seconds
            Console.Write("Safe exit procedure complete... Exiting in: ");
            for (int i = 3; i >= 0; i--)
            {
                Console.Write(i.ToString());
                Thread.Sleep(1000);
                Console.Write("\b");
            }

            // Let the bot close
            exit = true;
        }

        public static string HexStringFromBytes(byte[] bytes)
        {
            var sb = new StringBuilder();
            foreach (byte b in bytes)
            {
                var hex = b.ToString("x2");
                sb.Append(hex);
            }
            return sb.ToString();
        }

        public static double getUnixTimestamp(int offset)
        {
            return (Math.Round((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds, 0) + offset);
        }
    }
}
